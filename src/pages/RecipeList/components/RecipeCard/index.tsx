import * as React from 'react'
import { IRecipe } from 'contentful/types'
import styled from '@emotion/styled'
import { theme } from 'theme'
import { Link } from '@reach/router'
import Calories from 'components/Calories'

const Container = styled(Link)`
    display: flex;
    width: 100%;
    min-height: 320px;
    overflow: hidden;
    cursor: pointer;
    transition: .2s transform ease-in;
    transform: scale(1);
    border-bottom: 1px solid #dedede;
    border-radius: 0px;
    &:hover{
        transition: .2s transform ease-in, .2s transform ease-in;
        transform: scale(1.02);
        z-index: 1;
        border-radius: 4px;
        overflow: hidden;
    }
`

const Details = styled.div`
    display: flex;
    align-items: center;
    padding: ${ theme.spacing( 1, 6 ) };
    background: rgba( 255, 255, 255, 0.92 );
    flex-basis: 60%;
    font-siize: 32px;
    font-size: 32px;
`

const Cover = styled.img`
    object-fit: cover;
    height: 100%;
    width: 100%;
`

const CoverContainer = styled.div`
    position: relative;
    flex-basis: 40%;
    max-width: 40%;
    width: 40%;
    border-right: 1px solid #fff;
    background: #dedede;
`

const CaloriesContainer = styled.div`
    position: absolute;
    bottom: 0;
    right: 0;
    margin: 0;
    padding: ${ theme.spacing( 4 ) };
`

interface Props {
    recipe: IRecipe
}

const RecipeCard: React.FC<Props> = ( {
    recipe
} ) => {

    const { title, calories, id, photo: {
        file: {
            url: imageUrl,
        }
    } } = recipe 

    return (
        <Container to={`/recipe/${ id }`}>
            <CoverContainer>
                <Cover src={ imageUrl } />
                <CaloriesContainer>
                    <Calories calories={ calories } />
                </CaloriesContainer>
            </CoverContainer>
            <Details>
                { title }
            </Details>
        </Container>
    )
}

export default React.memo( RecipeCard )
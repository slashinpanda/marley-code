import { IRecipe } from 'contentful/types'
import * as React from 'react'
import RecipeCard from './components/RecipeCard'
import Container from 'components/Container'
import styled from '@emotion/styled'

const CardContainer = styled(Container)`
    display: flex;
    flex-wrap: wrap;
`

interface Props {
    recipes: IRecipe[],
}

const RecipeList: React.FC<Props> = ( { recipes } ) => {
    return (
        <CardContainer>
            { recipes.map( recipe => {
                return (
                    <RecipeCard recipe={ recipe } />
                )
            } ) }
        </CardContainer>
    )
}

export default React.memo( RecipeList )

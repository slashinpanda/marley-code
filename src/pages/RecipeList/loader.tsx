import * as React from 'react'
import { getRecipes } from 'contentful/recipes'
import { RouteComponentProps } from '@reach/router'
import LoadingPlaceholder from 'components/Loader'
import ErrorMessage from 'components/ErrorMessage'
import { ErrorBoundary } from 'components/ErrorBoundary'

// lazy load recipe list and query recipes in parallel
const Loader = React.lazy( () => {
    return Promise.all( [
        import( './index' ),
        getRecipes()
    ] )
    .then( ( [ { default: Component }, recipes ] ) => {
        return {
            default: React.memo( () => {
                return (
                    <Component recipes={recipes} />
                )
            } )
        }
    } )
    .catch( Promise.reject )
} )

const Error = React.memo( () => {
    return (
        <ErrorMessage message='failed to load recipes' />
    )
} )

// display a fallback
const RecipeListLoader: React.FC<RouteComponentProps> = () => {
    return (
        <ErrorBoundary fallback={<Error />}>
            <React.Suspense fallback={ <LoadingPlaceholder /> }>
                <Loader />
            </React.Suspense>
        </ErrorBoundary>
    )
}

export default React.memo( RecipeListLoader )
import * as React from 'react'
import { getRecipe } from 'contentful/recipes'
import { RouteComponentProps } from '@reach/router'
import LoadingPlaceholder from 'components/Loader'
import { ErrorBoundary } from 'components/ErrorBoundary'
import ErrorMessage from 'components/ErrorMessage'


// lazy load recipe list and query recipes in parallel
const loadRecipe =  ( recipeId: string ) => React.lazy( () => {
    return Promise.all( [
        import( './index' ),
        getRecipe( recipeId )
    ] )
    .then( ( [ { default: Component }, recipe ] ) => {
        return {
            default: React.memo( () => {
                return (
                    <Component recipe={ recipe } />
                )
            } )
        }
    } )
    .catch( Promise.reject )
} )

const Error = React.memo( () => {
    return (
        <ErrorMessage homeBtn message='failed to load recipe' />
    )
} )


// display a fallback
const RecipeLoader: React.FC<RouteComponentProps<{
    recipeId: string,
}>> = ( { recipeId } ) => {

    const Loader = React.useMemo( () => {
        return loadRecipe( recipeId! )
    }, [ recipeId ] )

    return (
        <ErrorBoundary fallback={<Error />}>
            <React.Suspense fallback={ <LoadingPlaceholder /> }>
                <Loader />
            </React.Suspense>
        </ErrorBoundary>
    )
}

export default React.memo( RecipeLoader )
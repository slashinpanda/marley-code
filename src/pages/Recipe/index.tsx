import styled from '@emotion/styled'
import { Link } from '@reach/router'
import Container from 'components/Container'
import { IRecipe } from 'contentful/types'
import * as React from 'react'
import { theme } from 'theme'
import * as DOMPurify from 'dompurify'
import marked from 'marked'
import Button from 'components/Button'
import Calories from 'components/Calories'

const CoverContainer = styled.div`
    display: block;
    position: relative;
    border-radius: 4px 4px 0 0;
    overflow: hidden;
`

const Cover = styled.img`
    display: block;
    height: 460px;
    width: 100%;
    object-fit: cover;
`

const CoverBottom = styled.div`
    padding: ${ theme.spacing( 4,8 ) };
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    background: linear-gradient( to bottom, rgba(0, 0, 0, 0), rgba( 0, 0, 0, 0.72 ) );
    color: white;
`

const Title = styled.h1`
    font-size: 3.2rem;
    font-weight: 400;
    margin: ${ theme.spacing( 0, 0, 2 ) };
`

const Content = styled.div`
    background: white;
    padding: ${ theme.spacing( 4, 8 ) };
`

const CoverTop = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    margin: ${ theme.spacing( 4,8 ) };
`

const HTML = styled.div`
    margin: ${ theme.spacing(0, 0, 4) };
    text-indent: 2rem;
    text-align: justify;
    font-size: 1.8rem;
    a {
        color: ${ theme.colors.primary.toString() };
    }
`

const Tags = styled.div`
    color: ${ theme.colors.primary.toString() };
    text-shadow: 0px 1px 0px rgba( 0, 0, 0, 0.52 );
    margin: ${ theme.spacing( 0, 0, 2 ) };
    &:empty {
        display: none;
    }
`

const ChiefContainer = styled.div`
    margin: ${ theme.spacing( 4, 0, 8 ) };
    background: ${ theme.colors.primary.alpha(0.12).toString() };
    padding: ${ theme.spacing( 4 ) };
    border-radius: 4px;
    i {
        font-size: 3.2rem;
        font-style: normal;
        margin: ${ theme.spacing( 0, 4, 0, 0 ) };
        vertical-align: middle;
    }
`

const ChefName = styled.span`
    vertical-align: middle;
    font-size: 1.8rem;
    font-weight: bold;
    opacity: .72;
`

interface Props {
    recipe: IRecipe;
}

const Recipe: React.FC<Props> = ( { recipe } ) => {
    const { title, photo: { file: { url } }, description, tags, chef, calories } = recipe

    const contents = React.useMemo( () => {
        return DOMPurify.sanitize( marked( description ) )
    }, [ description ] )

    const tagsText = React.useMemo( () => {
        return tags.map( ( { name } ) => name )
            .join( ', ' )
    }, [ tags ] )

    return (
        <Container>
            <CoverContainer>
                    <Cover alt={ title } src={ url } />
                    <CoverTop>
                        <Link to='/'>
                            <Button type='button'>all recipes</Button>
                        </Link>
                    </CoverTop>
                    <CoverBottom>
                        <Title>
                            { title }
                        </Title>
                        <Tags>
                            { tagsText }
                        </Tags>
                        <Calories calories={ calories } />
                    </CoverBottom>
            </CoverContainer>
            <Content>
                {
                    chef ? (
                        <ChiefContainer>
                            <i>
                                👨‍🍳
                            </i>
                            <ChefName>
                                { chef.name }
                            </ChefName>
                        </ChiefContainer>
                    ) : null
                }
                <HTML dangerouslySetInnerHTML={ { __html: contents } } />
            </Content>
        </Container>
    )
}

export default React.memo( Recipe )
import React from 'react';
import { render, screen } from '@testing-library/react';
import Recipe from './index';
import { IRecipe } from 'contentful/types';

// mock data
const recipeData: IRecipe = {
    id: 'a',
    tags: [
        {
            id: 'b',
            name: 'meat',
        },
        {
            id: 'c',
            name: 'more meat',
        },
    ],
    title: 'Red hot chili peppers',
    calories: 33,
    description: `\
*Grilled Cheese 101*: Use delicious cheese and good quality bread; make crunchy on the outside and ooey gooey on the inside; add one or two ingredients for a flavor punch!
In this case, cherry preserves serve as a sweet contrast to cheddar cheese, and basil adds a light, refreshing note. Use __mayonnaise__ on the outside of the bread to achieve
the ultimate, crispy, golden-brown __grilled cheese__. Cook, relax, and enjoy!
    `,
    chef: {
        id: 'd',
        name: 'Frusciante',
    },
    photo: {
        file: {
            url: 'hello.jpg',
            contentType: 'image/jpeg',
            details: {
                image: {
                    width: 300,
                    height: 300,
                }
            }
        },
    }
}

test('renders some recipe data and markdown', () => {

    render(<Recipe recipe={ recipeData } />);

    const titleElem = screen.getByText(
        new RegExp( recipeData.title, 'i' )
    );
    expect(titleElem).toBeInTheDocument();

    const caloriesElem = screen.getByText(
        new RegExp( recipeData.calories.toString(), 'i' )
    );
    expect(caloriesElem).toBeInTheDocument();

    const chefNameElem = screen.getByText(
        new RegExp( recipeData.chef!.name, 'i' )
    );
    expect(chefNameElem).toBeInTheDocument();


    const markdownElem = screen.getByText(
        new RegExp( 'Grilled Cheese 101', 'i' )
    );
    expect(markdownElem.nodeName).toEqual( 'EM' );

});

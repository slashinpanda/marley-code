export class Color {
    r: number;
    g: number;
    b: number;
    a: number;
    constructor( r: number, g: number, b: number, a = 1 ) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    static __shade( color: number, fac: number ): number {
		return color * ( 1 - fac )
	}

	static __tint( color: number, fac: number ): number {
		return Math.min( Math.round( color * ( 255 - color ) * fac ), 255 )
	}

    alpha( a: number ): Color {
		const { r, g, b } = this
		return new Color( r, g, b, a )
	}

	shade( fac: number ): Color {
		return new Color(
			Color.__shade( this.r, fac ),
			Color.__shade( this.g, fac ),
			Color.__shade( this.b, fac ),
		)
	}

	tint( fac: number ): Color {
		return new Color(
			Color.__tint( this.r, fac ),
			Color.__tint( this.g, fac ),
			Color.__tint( this.b, fac ),
		)
	}

    toString() {
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
    }
}
import { Theme } from "@emotion/react"
import { Color } from "./color"

const Multiplier = 4

// separated into 2 functions (`space` and `spacing`) to avoid re-declaration of the function below
// for each `spacing` call
const space = ( num: number ) => `${num * Multiplier}px`

const spacing = ( ...args: number[] ) =>
    args.map( space ).join( ' ' )

export const theme: Theme = {
    colors: {
        primary: new Color( 46, 204, 113 ),
    },
    spacing,
}
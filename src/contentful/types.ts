// interface for contentful content types received from contentful client
export interface IDirtyContentType<ContentType> {
    fields: ContentType,
    sys: {
        id: string,
    }
}

// recipe > tag
export interface ITag {
    name: string;
    id: string;
}

// recipe > chef
export interface IChef {
    name: string;
    id: string;
}

// contentful assets, images so far
export interface ImageAsset {
    file: {
        contentType: string,
        url: string,
        details: {
            image: {
                width: number,
                height: number,
            }
        }
    },
}

// recipes received from contentful client
export interface IDirtyRecipe {
    calories: number,
    title: string;
    description: string,
    photo: IDirtyContentType<ImageAsset>,
    tags?: IDirtyContentType<ITag>[],
    chef?: IDirtyContentType<IChef>,
}

// internal interface for recipes
export interface IRecipe {
    calories: number,
    title: string;
    description: string;
    photo: ImageAsset,
    tags: ITag[],
    chef?: IChef,
    id: string;
}
import Client from './client'
import { IDirtyContentType, IDirtyRecipe, ImageAsset, IRecipe, ITag, IChef } from './types'

/**
 * Some functions to map content types to some internal interfaces
 */

const mapPhoto = ( dirty: IDirtyRecipe['photo'] ): ImageAsset => {
    return dirty.fields
}

const mapChef = ( {
    fields: {
        name,
    },
    sys: {
        id,
    }
} : IDirtyContentType<IChef> ) => ( {
    name,
    id,
} )

const mapTag = ( {
    fields: {
        name
    },
    sys: {
        id,
    }
}: IDirtyContentType<ITag> ): ITag => ( {
        name,
        id
    }
)

const mapRecipe = ( {
    fields: {
        calories,
        description,
        title,
        photo,
        tags,
        chef,
    },
    sys: {
        id,
    }
}: IDirtyContentType<IDirtyRecipe> ): IRecipe => ( {
    calories,
    description,
    title,
    photo: mapPhoto( photo ),
    tags: tags?.map( mapTag ) || [],
    chef: chef && mapChef( chef ),
    id,
} )

// get all recipes
// would have added a pagination but it looks there are only 4 recipes
export const getRecipes = (): Promise<IRecipe[]> => {
    return new Promise( ( resolve, reject ) => {
        return Client.getEntries<IDirtyRecipe>( {
            content_type:       'recipe',
            skip:               0,
            limit:              10,
        } )
        .then( data => resolve( data.items.map( mapRecipe ) ) )
        .catch( reject )
    } )
}

// get recipe by id
export const getRecipe = ( recipeId: string ): Promise<IRecipe> => {
    return new Promise( ( resolve, reject ) => {
        return Client.getEntry<IDirtyRecipe>( recipeId )
            .then( data => resolve( mapRecipe( data ) ) )
            .catch( reject )
    } )
}
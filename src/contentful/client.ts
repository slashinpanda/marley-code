import { createClient } from 'contentful'
import { CONTENTFUL_SPACE_ID, CONTENTFUL_TOKEN } from 'settings/env'

const Client = createClient( {
    space:          CONTENTFUL_SPACE_ID!,
    accessToken:    CONTENTFUL_TOKEN!,
} )

export default Client
import '@emotion/react';
import { Color } from 'theme/color';

declare module '@emotion/react' {
    export interface Theme {
        colors: {
            primary: Color;
        };
        spacing: (...args: number[]) => string;
    }
}

import styled from '@emotion/styled'
import { Link } from '@reach/router'
import * as React from 'react'
import { theme } from 'theme'

const Error = styled.div`
    font-size: 3.2rem;
    position: absolute;
    text-align: center;
    width: 320px;
    max-width: 100%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: ${ theme.spacing( 4, 6 ) };
    color: white;
    background: rgba( 0, 0, 0, .72 );
    border-radius: 4px;
    i {
        font-style: normal;
        margin: ${ theme.spacing( 0, 4 ) };
    }
`

const HomeLink = styled(Link)`
    color: ${ theme.colors.primary.toString() };
    font-size: 1.6rem;
`

const ErrorTop = styled.div`
    display: flex;
    justify-content: center;
`

interface Props {
    message: string;
    homeBtn?: boolean;
}

const ErrorMessage: React.FC<Props> = ( { message, homeBtn } ) => {

    return (
        <Error>
            <ErrorTop>
                <i>🚨</i>{ message }<i>🚨</i>
            </ErrorTop>
            { homeBtn ? (
                <>
                    <br />
                    <HomeLink to='/'>back home</HomeLink>
                </>
            ) : null }
        </Error>
    )

}

export default React.memo( ErrorMessage )
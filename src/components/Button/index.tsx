import styled from "@emotion/styled";
import { theme } from "theme";

const Button = styled.button`
    padding: ${ theme.spacing( 0, 2 ) };
    background: ${ theme.colors.primary.toString() };
    border: none;
    color: white;
    line-height: 3.6rem;
    border-radius: 4px;
    font-size: 1.4rem;
    font-weight: bold;
    cursor: pointer;
    transition: .2s background ease-in;
    &:hover {
        transition: .2s background ease-in;
        background: ${ theme.colors.primary.shade(0.1).toString() };
    }
`

export default Button
import styled from '@emotion/styled'
import * as React from 'react'

const Text = styled.span`
    color: transparent;
    text-shadow: 0 0 0 white, 0 1px 0 rgba(0, 0, 0, .72);
    font-size: 2.4rem;
`

interface Props {
    calories: number;
}

const Calories: React.FC<Props> = ( { calories } ) => {

    return (
        <Text>🔥 { calories }</Text>
    )

}

export default React.memo( Calories )
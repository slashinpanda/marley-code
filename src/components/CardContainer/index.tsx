import * as React from 'react'
import styled from '@emotion/styled'
import Container from '../Container'

const Card = styled.div`
    background: rgba(255, 255, 255, 0.92);
    padding: 24px;
`

interface Props {
    children?: React.ReactNode,
}

const CardContainer: React.FC<Props> = ( { children } ) => {
    return (
        <Container>
            <Card>
                { children }
            </Card>
        </Container>
    )
}

export default React.memo( CardContainer )
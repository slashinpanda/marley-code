import styled from '@emotion/styled'

const Container = styled.div`
    margin: 0 auto;
    width: 1200px;
    max-width: 100%;
`

export default Container
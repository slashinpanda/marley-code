import * as React from 'react'
import styled from "@emotion/styled";

const Text = styled.div`
    font-size: 54px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    letter-spacing: 12px;
`

const emojis = Array.from( '🍇🍈🍉🍊🍋🍌🍍🥭🍎🍏🍐🍑🍒🍓🫐🥝🍅🫒🥥🥑🍆🥔🥕🌽🌶️🫑🥒🥬🥦🧄🧅🍄🥜🌰🍞🥐🥖🫓🥨🥯🥞🧇🧀🍖🍗🥩🥓🍔🍟🍕🌭🥪🌮🌯🫔🥙🧆🥚🍳🥘🍲🫕🥣🥗🍿🧈🧂🥫🍱🍘🍙🍚🍛🍜🍝🍠🍢🍣🍤🍥🥮🍡🥟🥠🥡🦪🍦🍧🍨🍩🍪🎂🍰🧁🥧🍫🍬🍭🍮🍯🍼🥛☕🫖🍵🍶🍾🍷🍸🍹🍺🍻🥂🥃🥤🧋🧃🧉' )

// will display some random emojis
const LoadingPlaceholder = () => {

    const text = React.useMemo( () => {
        const copy = emojis.slice()
        let length = emojis.length
        return [
            copy.splice( Math.floor( Math.random() * length ), 1 ), 
            copy.splice( Math.floor( Math.random() * --length ), 1 ),
            copy.splice( Math.floor( Math.random() * --length ), 1 ),
            copy.splice( Math.floor( Math.random() * --length ), 1 ),
        ]

    }, [] )

    return (
        <Text>{text}</Text>
    )

}

export default React.memo( LoadingPlaceholder )

import styled from '@emotion/styled'
import * as React from 'react'
import { theme } from 'theme'

const Container = styled.div`
    margin: ${ theme.spacing(8, 0, 0) }
`

interface Props {
    children: React.ReactNode,
}

const Layout: React.FC<Props> = ( { children } ) => {

    return (
        <Container>
            { children }
        </Container>
    )

}


export default React.memo( Layout )
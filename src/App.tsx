import React from 'react';
import './App.css';
import { Router } from '@reach/router'
import RecipeList from 'pages/RecipeList/loader'
import Recipe from 'pages/Recipe/loder'
import Layout from 'components/Layout';

function App() {

  return (
    <Layout>
      <Router>
        <Recipe path='/recipe/:recipeId' />
        <RecipeList default />
      </Router>
    </Layout>
  );
}

export default App;
